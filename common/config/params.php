<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'storagePath' => '@backend/web/uploads/',  
    'storageUri' => 'http://blogbackend.com/uploads/',  //для отображения на сайте путь.
    
    'maxFileSize' => 1024 * 1024 * 2, // 2 megabites
    'storageUri' => '/uploads/',   // http://imagesfront.com/uploads/f1/d7/739f9a9c9a99294.jpg

    'blogPicture' => [
        'maxWidth' => 100,
        'maxHeight' => 100,
    ],
    
];
