<?php

namespace frontend\components;

use yii\base\Component;


/**
 * Cache component
 *
 * @author admin
 */

class Cache extends Component  

{
    const CACHE = __DIR__ . '/../../frontend/tmp/cache';

    /**
     * Set Cache and get Cache
     */
    public function set($key, $data, $seconds = 600)
    {
        $content['data'] = $data;
        $content['end_time'] = time() + $seconds;
       if(file_put_contents(self::CACHE . '/' . md5($key) . '.txt', serialize($content))) {
           return true;
       }
       return false;
        
    }
    
    public function get($key)
    {
        $file = self::CACHE . '/' . md5($key) . '.txt';
        if(file_exists($file)) {
            $content = unserialize(file_get_contents($file));
            if(time() <= $content['end_time']) {
               return $content['data']; 
            }
            unlink($file);
        }
       return false;
    }
    
    public function delete($key)
    {
        $file = self::CACHE . '/' . md5($key) . '.txt';
        if(file_exists($file)) {
            unlink($file);
        }
       return false;
    }

}