<?php

namespace frontend\models;

use Yii; 
use frontend\models\CurrencyRates;
use yii\web\NotFoundHttpException;

class Course extends \yii\base\Model
{
    public $created_at;
    public $id_currency;
    public $rates;
    
    const LINK = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=3";
    
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['rates'], 'number'],
            [['id_currency'], 'string', 'max' => 255],
        ];
    }
    
    public static function getCourse()
    {
        $link = file_get_contents(self::LINK);   
        if(!$link) return false;
        
        //$courses = json_decode($link, true); //асоциативный массив
        $courses = json_decode($link);  // обект
        
        return $courses; 
    }
    
    
    public static function findHistory($history)
    {
        if (($model = CurrencyRates::findLastCourse($history)) !== null) {

            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function insert_course($course_curr, $ccy)
    {

        if (!$this->validate()) {
            return null;
        }
        
        $currency_rates = new CurrencyRates();
        
        $currency_rates->created_at = time();
        $currency_rates->id_currency = $ccy;
        $currency_rates->rates  = $course_curr;

        return $currency_rates->save() ? $currency_rates : null;
        
    }
    
    
    
}
