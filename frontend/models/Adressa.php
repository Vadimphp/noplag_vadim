<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "adressa".
 *
 * @property int $id
 * @property int $user_id
 * @property int $post_index
 * @property string $country
 * @property string $city
 * @property string $street
 * @property int $house_namber
 * @property int $office
 *
 * @property User $user
 */
class Adressa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adressa';
    }
    
     public function rules()
    {
        return [
            ['user_id', 'trim'],
            
            [['post_index', 'country', 'city', 'street', 'house_namber'], 'trim'],
            [['post_index', 'country', 'city', 'street', 'house_namber'], 'required'],
            
            ['post_index', 'number'],
            
            ['country', 'string', 'min' => 2, 'max' => 2],
            ['country', 'filter', 'filter' => function ($value) {
		    $value = strtoupper($value); 
                    return $value;
            }],
                    
            ['office', 'trim'],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'post_index' => 'Post Index',
            'country' => 'Country',
            'city' => 'City',
            'street' => 'Street',
            'house_namber' => 'House Namber',
            'office' => 'Office',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
