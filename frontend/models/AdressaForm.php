<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use frontend\models\Adressa;

/**
 * Signup form
 */
class AdressaForm extends Model
{
    
    public $user_id;
    public $post_index;
    public $country;
    public $city;
    public $street;
    public $house_namber;
    public $office;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['user_id', 'trim'],
            
            [['post_index', 'country', 'city', 'street', 'house_namber'], 'trim'],
            [['post_index', 'country', 'city', 'street', 'house_namber'], 'required'],
            
            ['post_index', 'number'],
            
            ['country', 'string', 'min' => 2, 'max' => 2],
            ['country', 'filter', 'filter' => function ($value) {
		    $value = strtoupper($value); 
                    return $value;
            }],
                    
            ['office', 'trim'],

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function adressUp()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $adress = new Adressa();

        $id = $this->getId($this->user_id);
    
        $adress->user_id = $id->id;
        
        $adress->post_index = $this->post_index;
        $adress->country = $this->country;
        $adress->city  = $this->city;
        $adress->street  = $this->street;
        $adress->house_namber  = $this->house_namber;
        $adress->office  = $this->office;

        return $adress->save() ? $adress : null;

    }
    
    public function add($id)
    {
        if (!$this->validate()) {
            return null;
        }
        
        $adress = new Adressa();

        //$id = $this->getId($this->user_id);
       
        $adress->user_id = $id;
        
        $adress->post_index = $this->post_index;
        $adress->country = $this->country;
        $adress->city  = $this->city;
        $adress->street  = $this->street;
        $adress->house_namber  = $this->house_namber;
        $adress->office  = $this->office;

        return $adress->save() ? $adress : null;

    }
    
    public static function getId($email)
    {

        return User::find()->where(['email' => $email])->one();
        //return User::find()->limit(1)->orderBy(['id' => SORT_DESC])->one();
    }
    
    
}
