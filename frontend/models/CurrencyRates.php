<?php

namespace frontend\models;

use Yii;


/**
 * This is the model class for table "currency_rates".
 *
 * @property int $id
 * @property string $created_at
 * @property string $id_currency
 * @property double $rates
 */
class CurrencyRates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency_rates';
    }
    
     public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_at'], 'safe'],
            [['rates'], 'number'],
            [['id_currency'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Date',
            'id_currency' => 'Id Currency',
            'rates' => 'Rates',
        ];
    }
    
    public static function findLastCourse ($history){
        
        return static::find()
            ->where(['id_currency' => $history])
            ->orderBy('created_at DESC')
            ->one();
        
    }
    
    
    public static function findHistoryCourses()
    {
        
        $order = ['created_at' => SORT_DESC];
        
        return static::find()->orderBy($order)->all();
    }
}
