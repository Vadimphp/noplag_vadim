<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const GENDER = "Он/а";
    
    public $username;
    public $email;
    public $password;
    public $login;
    public $lastname;
    public $gender;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
               
            [['username', 'login'], 'trim'],
            [['username', 'login'], 'required'],
            [['username', 'login'], 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['login', 'string', 'min' => 4],
            
            
            [['username', 'lastname'], 'filter', 'filter' => function ($value) {
		    $value = ucfirst($value); 
                    return $value;
            }],
            

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
                    
            ['gender', 'default', 'value' => self::GENDER], 
                    
                    
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        
        $user->login = $this->login;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->username = $this->username;
        $user->gender = $this->gender;
        $user->lastname = $this->lastname;
        $user->email = $this->email;       
        
              
        return $user->save() ? $user : null;
        
    }
}
