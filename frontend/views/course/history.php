<?php

/* @var $this yii\web\View */
/* @var $get_history frontend\models\CurrencyRates */


$this->title = 'My Yii Application';
?>
<div class="site-index">



    <div class="body-content">

            <div class="row">
                <div class="col-lg-12">
                
                    <p>Exchange Rates</p>
             

                </div>
           
            </div>
                
        <div class="table-responsive">
        
            <table class="table table-hover table-striped">
            
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Currency</th>
                        <th>Value</th>
                    </tr>
                </thead>
            
                <tbody>
               
                    <?php if (!empty($get_history)) : ?>
                        <?php foreach ($get_history as $history): ?>
                        <tr> 
                            <td><?php echo Yii::$app->formatter->asDateTime($history->created_at); ?></td>
                            <td><?php echo $history->id_currency; ?></td>
                            <td><?php echo $history->rates; ?></td>              
                        </tr>
                        <?php endforeach?>
                    <?php endif; ?>

                
                </tbody>
            
            </table>
        
        </div>
                          

    </div>
</div>
