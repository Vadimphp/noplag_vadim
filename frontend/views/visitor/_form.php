<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adress-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login')->textInput() ?>           

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            
        <?= $form->field($model, 'lastname')->textInput()->hint('Пожалуйста, введите фамилию')->label('Last Name') ?>            

        <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
