<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            
                <?= $form->field($model, 'login')->textInput() ?>
            
                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            
                <?= $form->field($model, 'lastname')->textInput()->hint('Пожалуйста, введите фамилию')->label('Last Name') ?>
            
                <?= $form->field($model, 'gender')->radioList( [0 => 'мужской', 1 => 'женский'] ) ?>

                <?= $form->field($model, 'email') ?>

               

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    
    <br>
     <br>
     <br>
     <br>
    <div class="row">
        <div class="col-lg-5">
            <?php $form2 = ActiveForm::begin(['id' => 'form-adress']); ?>
            
            <?php if($model->email): ?>
                <?= $form2->field($model2, 'user_id')->hiddenInput(['value'=> $model->email])->label(false); ?>
            <?php endif; ?>
            
                <?= $form2->field($model2, 'post_index')->textInput()->hint('Пожалуйста, введите только цифры') ?>
    
                <?= $form2->field($model2, 'country')->textInput()->hint('Пожалуйста, введите двух буквенный код') ?>

                <?= $form2->field($model2, 'city')->textInput() ?>
            
                <?= $form2->field($model2, 'street')->textInput() ?>
    
                <?= $form2->field($model2, 'house_namber')->textInput() ?>

                <?= $form2->field($model2, 'office')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Add adress', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
     
</div>
