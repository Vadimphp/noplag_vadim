<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">        
        <div class="body-content">
        
            <div class="row">
        
                <?php Pjax::begin(['id' => 'item']); ?>   
    
                    <?php
                         echo ListView::widget([
                            'dataProvider' => $dataProvider,                       
                            'layout' => "{pager}\n{items}",
                            'itemView' => '_about',
                                'options' => [
                                'tag' => 'div',
                                'class' => 'text-center',
                                'id' => 'pager', // не заполнял
                            ],
                        ]);
                    ?>
    
                <?php Pjax::end(); ?>
            </div>
            
        
        </div>           
    </div>
    
</div>
