<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adress-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'post_index')->textInput()->hint('Пожалуйста, введите только цифры') ?>
    
        <?= $form->field($model, 'country')->textInput()->hint('Пожалуйста, введите двух буквенный код') ?>

        <?= $form->field($model, 'city')->textInput() ?>
            
        <?= $form->field($model, 'street')->textInput() ?>
    
        <?= $form->field($model, 'house_namber')->textInput() ?>

        <?= $form->field($model, 'office')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
