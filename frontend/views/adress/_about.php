<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="col-md-4">

    <h4><?php echo 'id - ' . Html::encode($model->id); ?></h4>
    <p><?php echo 'пользователь - ' . Html::encode($model->username); ?></p>
    <p><?php echo 'email - ' . Html::encode($model->email); ?></p>                           
    <span><?php echo Yii::$app->formatter->asDateTime($model->created_at); ?></span>
    <br>
    <?php if (Yii::$app->user->identity && Yii::$app->user->identity->equals($model)): ?>
        <a href="<?php echo Url::to(['adress/view', 'id' => $model->id]); ?>">
            <h6>Просмотр</h6>
        </a>
        <a href="<?php echo Url::to(['visitor/update', 'id' => $model->id]); ?>">
            <h6>Редактировать</h6>
        </a>
        <a href="<?php echo Url::to(['visitor/delete', 'id' => $model->id]); ?>">
            <h6>Удалить</h6>
        </a>
    <?php else: ?>
    
        <a href="<?php echo Url::to(['adress/view', 'id' => $model->id]); ?>">
            <h6>Просмотр</h6>
        </a>
    
    <?php endif; ?>
    
    <hr>

</div>