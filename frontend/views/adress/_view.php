<?php

use yii\helpers\Url;
use yii\helpers\Html;
/**
 *  @var Product
 *  @var get item from backend and other parameters for one product and put it in dataProvider 
 */
?>

    <div class="col-md-4">

        <h4><?php echo 'id - ' . Html::encode($model->id); ?></h4>
        <p><?php echo 'user_id - ' . Html::encode($model->user_id); ?></p>
        <p><?php echo 'почтовый индекс - ' . Html::encode($model->post_index); ?></p>                           
        <p><?php echo 'страна  - ' . Html::encode($model->country); ?></p>
        <p><?php echo 'город - ' . Html::encode($model->city); ?></p>
        <p><?php echo 'страна - ' . Html::encode($model->street); ?></p>
        <p><?php echo 'номер дома - ' . Html::encode($model->house_namber); ?></p>
        <p><?php echo 'номер офиса/квартиры - ' . Html::encode($model->office); ?></p>
        
        <br>
        <?php if (Yii::$app->user->identity && Yii::$app->user->identity->equals($model->user)): ?>
        <a href="<?php echo Url::to(['adress/update', 'id' => $model->id]); ?>">
            <h6>Редактировать</h6>
        </a>
        <a href="<?php echo Url::to(['adress/delete', 'id' => $model->id]); ?>">
            <h6>Удалить</h6>
        </a>
        <a href="<?php echo Url::to(['adress/create', 'id' => $model->user_id]); ?>">
            <h6>Создать</h6>
        </a>
        <hr> 
        <?php endif; ?>

    </div>