<?php


use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ListView;

?>


<div class="container">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                
            <?php Pjax::begin(['id' => 'item']); ?> 
                
                <h4><?php echo 'id - ' . Html::encode($user->id); ?></h4>
                <p><?php echo 'пользователь - ' . Html::encode($user->username); ?></p>
                <p><?php echo 'email - ' . Html::encode($user->email); ?></p>                           
                <span><?php echo Yii::$app->formatter->asDateTime($user->created_at); ?></span>
                <br>
                <hr>
                                                
            </div>          
        </div>
        
        <div class="row">
                     
            <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider,                       
                    'layout' => "{pager}\n{items}",
                    'itemView' => '_view',
                    'options' => [
                        'tag' => 'div',
                        'class' => 'text-center',
                        'id' => 'pager', // не заполнял
                    ],
                ]);
                ?>
            
            <?php Pjax::end(); ?>
                
       
        </div>

    </div>
</div>


