<?php

namespace frontend\controllers;

use Yii; 
use yii\web\Controller;
use frontend\models\Adressa;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use frontend\models\AdressaForm;


class AdressController extends Controller
{
    
    public function actionAbout()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'pagination' => [
                'pageSize' => 4,
            ]
        ]);
        
        return $this->render('about', [
            'dataProvider' => $dataProvider,            
        ]);
    }
    

    
    public function actionView($id) 
    {   
        
        $user = User::find()->where(['id' => $id])->one();
        
        $dataProvider = new ActiveDataProvider([
            'query' => Adressa::find()->where(['user_id' => $id]),
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render('view', [
            'user' => $user,
            'dataProvider' => $dataProvider,
            
        ]);
    }
     public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        
        if ($model->load(Yii::$app->request->post()) ) {               

            if ($model->save()) {
                
                Yii::$app->session->setFlash('success', 'Adress update!');
                
                return $this->redirect(['adress/about']);
                
            }
        }  

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['adress/about']);
    }
    
    public function actionCreate($id)
    {
        
        $model = new AdressaForm();
        
         if ($model->load(Yii::$app->request->post()) && $model->add($id)) {
            //return $this->redirect(['adress/view', 'id' => $model->user_id]);
            return $this->redirect(['adress/about']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    
    protected function findModel($id)
    {
        if (($model = Adressa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    

}
