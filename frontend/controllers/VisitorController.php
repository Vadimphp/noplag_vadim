<?php

namespace frontend\controllers;

use Yii; 
use yii\web\Controller;
use common\models\User;
use yii\web\NotFoundHttpException;



class VisitorController extends Controller
{
    

     public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        
        if ($model->load(Yii::$app->request->post()) ) {               

            if ($model->save()) {
                
                Yii::$app->session->setFlash('success', 'Adress update!');
                
                return $this->redirect(['adress/about']);
                
            }
        }  

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['adress/about']);
    }
    

    
    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    

}
