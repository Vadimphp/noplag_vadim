<?php

namespace frontend\controllers;

use Yii; 
use yii\web\Controller;
use frontend\models\Course;
use frontend\models\CurrencyRates;
use yii\web\NotFoundHttpException;


class CourseController extends Controller
{
    
    public function actionIndex()
    {

        $model = new Course();
        //$courses = $model->getCourse();
        
        $courses = Yii::$app->cache->get('Exchange');
        if (!$courses) {
            $courses = $model->getCourse();
            Yii::$app->cache->set('Exchange', $courses);
        }
        
        return $this->render('index', [
            'courses' => $courses,
            'model' => $model,            
        ]);
    }
    
    
    
    
    protected function findModel()
    {
        if (($model = CurrencyRates::findLastCourse()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionHistory()
    {
        
        $get_history = CurrencyRates::findHistoryCourses();
        

        return $this->render('history', [
            'get_history' => $get_history,
                    
        ]);
    }
    
    
    

}
