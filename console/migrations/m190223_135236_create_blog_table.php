<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%blog}}`.
 */
class m190223_135236_create_blog_table extends Migration
{

    public function up()
    {
        $this->createTable('blog', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'blog_head' => $this->string(),
            'preview_text' => $this->text(),
            'text' => $this->text(),            
            'filename' => $this->string()->notNull(),
            'post_name' => $this->string(),

       
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blog');
    }

}