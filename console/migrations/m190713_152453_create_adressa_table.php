<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%adressa}}`.
 */
class m190713_152453_create_adressa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%adressa}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'post_index' => $this->integer(),
            'country' => $this->string(2)->notNull(),
            'city' => $this->text(),
            'street' => $this->text(),
            'house_namber' => $this->integer(),
            'office' => $this->integer(),      

        ]);
        
        $this->addForeignKey(
            'fk-adressa-user_id',
            'adressa',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%adressa}}');
    }
}
