<?php

use yii\db\Migration;

/**
 * Handles adding new to table `{{%user}}`.
 */
class m190713_150657_add_new_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'login', $this->string(70)->unique());
        $this->addColumn('{{%user}}', 'lastname', $this->string(70));
        $this->addColumn('{{%user}}', 'gender', $this->string(70));
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'login');
        $this->dropColumn('{{%user}}', 'lastname');
        $this->dropColumn('{{%user}}', 'gender');
        
    }
}
