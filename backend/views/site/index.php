<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>My Blog</h1>

        
        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
           <div class="col-md-12">
                <h2>Blog</h2>

                <p><a class="btn btn-default" href="<?php echo Url::to(['/blogadmin/manage']); ?>">blog info</a></p>
                
            </div>
        </div>

    </div>
</div>
