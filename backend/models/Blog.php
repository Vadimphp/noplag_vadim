<?php

namespace backend\models;

use yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;



/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property int $created_at
 * @property string $blog_head
 * @property string $preview_text
 * @property string $text
 * @property string $filename
 * @property string $post_name
 */
class Blog extends ActiveRecord
{
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

     /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'blog_head' => 'Blog Head',
            'preview_text' => 'Preview Text',
            'text' => 'Text',
            'filename' => 'Filename',
            'post_name' => 'Post Name',
        ];
    }
    
    public function getImage()
    {
        return Yii::$app->storage->getFile($this->filename); 
    }
    
}
