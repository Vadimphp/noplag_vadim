<?php

namespace backend\models\traits;

use Yii;
use Intervention\Image\ImageManager;

trait ResizePictureTrait  {
   
    
    public function resizePicture()
    {
        if ($this->filename->error) {
            /* В объекте UploadedFile есть свойство error. Если в нем "1", значит
            * произошла ошибка и работать с изображением не нужно, прерываем
            * выполнение метода */
            return;
        }
        
        $width = Yii::$app->params['blogPicture']['maxWidth'];
        $height = Yii::$app->params['blogPicture']['maxHeight'];

        $manager = new ImageManager(array('driver' => 'imagick'));

        $image = $manager->make($this->filename->tempName);     //    /tmp/11ro51  - получаем доступ к имени

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();  //сохранение пропорций
            $constraint->upsize(); //не изменять, если размер картинок меньше указаных к обрезке
        })->save();        //    /tmp/11ro51  //сохранять под таким же именем
    }
    
    
}
